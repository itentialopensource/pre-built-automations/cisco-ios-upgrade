
## 0.4.5 [07-12-2024]

Adds deprecation and metadata

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!60

2024-07-12 21:20:03 +0000

---

## 0.4.4 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!59

---

## 0.4.3 [06-27-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!55

---

## 0.4.2 [11-18-2021]

* Certify 2021.2 [DSUP-1046] package.json > IAPdependencies : removed automation catalog

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!54

---

## 0.4.1 [10-28-2021]

* 2021.2

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!53

---

## 0.4.0 [10-28-2021]

* Minor/dsup 961

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!52

---

## 0.3.12 [10-19-2021]

* Update JSON Form Health-check Fields

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!48

---

## 0.3.11 [07-09-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!47

---

## 0.3.10 [03-02-2021]

* patch/DSUP-886-fixing-jst-path

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!46

---

## 0.3.9 [03-02-2021]

* patch/DSUP-886-id-fix

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!44

---

## 0.3.8 [03-02-2021]

* Include retry for isAlive task

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!43

---

## 0.3.7 [01-05-2021]

* certifying IAP 2020.2

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!40

---

## 0.3.6 [11-11-2020]

* LB-502_507 - Added support for the flashMemory selection

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!39

---

## 0.3.5 [11-09-2020]

* LB-500_501

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!38

---

## 0.3.4 [10-08-2020]

* Updated README.md to change a mention of "IOS- XR" to "IOS"

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!37

---

## 0.3.3 [09-24-2020]

* Update bundles/transformations/iosUpgradeFormData.json, bundles/workflows/IAP...

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!36

---

## 0.3.2 [07-17-2020]

* added error handling during nso sync-from

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!35

---

## 0.3.1 [06-18-2020]

* [Patch/Readme] Update branch so readme is accurate

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!33

---

## 0.3.0 [06-18-2020]

* [minor/LB-404] Switch absolute path to relative path for img

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!30

---

## 0.2.0 [06-17-2020]

* Update package to include correct repo url and manifest to reflect gitlab name

See merge request itentialopensource/pre-built-automations/Cisco-IOS-Upgrade!29

---

## 0.1.13 [05-01-2020]

* removed app-artifacts from dependencies

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!28

---

## 0.1.12 [04-17-2020]

* Update IAP Artifacts IOS Upgrade - NSO upgrade.json

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!27

---

## 0.1.11 [04-17-2020]

* Update IAP Artifacts IOS Upgrade - NSO upgrade.json

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!27

---

## 0.1.10 [04-10-2020]

* Update IAP Artifacts IOS Upgrade - NSO upgrade.json

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!27

---

## 0.1.9 [04-10-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!25

---

## 0.1.8 [04-09-2020]

* Delete .gitlab-ci.yml

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!23

---

## 0.1.7 [04-07-2020]

* Deleted .gitlab-ci.yml

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!24

---

## 0.1.6 [04-07-2020]

* Update .gitlab-ci.yml

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!22

---

## 0.1.5 [03-30-2020]

* removed version from manifest.json

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!20

---

## 0.1.4 [03-10-2020]

* Update README.md

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!19

---

## 0.1.3 [02-20-2020]

* Patch/certify 2019.3

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!16

---

## 0.1.2 [01-30-2020]

* Adding TOC to README.md

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!8

---

## 0.1.1 [01-14-2020]

* Update .gitlab-ci.yml

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!7

---

## 0.1.0 [09-05-2019]

* Minor/ansible support

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!4

---

## 0.0.3 [08-29-2019]

* pamela.talley-master-patch-70542

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!2

---

## 0.0.2 [08-29-2019]

* pamela.talley-master-patch-70542

See merge request itentialopensource/pre-built-automations/cisco-ios-upgrade!2

---\n\n
