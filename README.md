## _Deprecation Notice_
This Pre-Built has been deprecated as of 07-12-2024 and will be end of life on 07-12-2025. The capabilities of this Pre-Built have been replaced by [Cisco - IOS - IAG](https://gitlab.com/itentialopensource/pre-built-automations/cisco-ios-iag).

## Table of Contents
  - [Overview](#overview)
  - [Features](#features)
  - [Installation Prerequisites](#installation-prerequisites)
  - [How to Install](#how-to-install)
  - [Supported Device Types](#supported-device-types)
  - [How to Run](#how-to-run)
  - [Additional Information](#additional-information)


## Overview
This pre-built contains the Cisco IOS device upgrade workflow for NSO and Ansible using IAP.
The workflow requires that a newer binary version file is already downloaded locally on the destination device (bootflash:), and file integrity has been verified (using md5).
To satisfy these pre-requirements, use the free [**file-transfer**](https://gitlab.com/itentialopensource/pre-built-automations/file-transfer) pre-built available from Itential.

This solution consist of the following:

* Main Workflow (**IAP-Artifacts IOS Device Upgrade**)
  * Perform device environmental checks. Verifies a device is on a different version than the requested one. 
  * Perform pre-checks to confirm device readiness.
  * Backup the running-config locally on flash drive.
  * Perform boot statement configuration to direct the router to load the newer version upon the next boot.
  * Issue the reload command.
  * Wait for device to become available after reboot.
  * Confirm reliable connectivity (ping consistency).
  * Perform post-checks to verify the device functionality running the new version.
  * Show a Pre-Post Checks diff report.
  * Perform MOP analysis to verify no unexpected config changes occurred.
  * Show a MOP analysis report.
  * Perform rollback, if requested.

* Command Templates
  * Will run the pre / post commands and evaluate them against set thresholds.

* Analytic Templates
  * Will run the pre vs. post comparisons and evaluate them against set thresholds.

* Automation Catalog Entry with a JSON-Form:
  * Mode selection: Zero-Touch, Normal, or Verbose
  * Allows user to pick destination device to run the upgrade on.
  * Allows user to pick software version to upgrade to (file names are hard coded in JSON form).
  * Ping-consistency variables 

## Features
  * Automatically find device orchestrator Ansible or NSO.
  * Perform readiness checks prior to any change, and functionality verifications after changes have been applied.
  * Allow for a rollback in case functionality checks have failed.
  * Show a conclusive report with the Pre vs. Post config diff.

## Installation Prerequisites

Users must satisfy the following prerequisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2023.1`

## How to Install

To install the pre-build:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section. 
* The pre-build can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-build and click the install button.

## Supported Device Types
  * Cisco IOS


## How to Run
Starting an IOS device upgrade can be invoked via Automation-Catalog. Navigate to the Automation-Catalog app and press run on the IAP Artifacts IOS Device Upgrade entry.



## Additional Information
Please use your Itential Customer Success Account if you need support when using this pre-built.
